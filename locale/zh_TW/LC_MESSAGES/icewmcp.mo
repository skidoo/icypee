��    :     �	  �  �      H     I     c  
   o  '   z     �     �     �     �  
   �     �     �     �     �            	        )  +   9  "   e     �     �  
   �     �  z   �     (  +   6  6   b     �     �  w   �     <     K     S     m     {     �     �     �     �     �  
   �  
   �     �                 $   #     H     O     a  '   s  F   �  �  �  �   �   z  N!  �   �"  o  �#     %     %     "%     )%     E%  	   _%     i%     p%     �%     �%     �%     �%  ,   �%  ,   &  +   D&  -   p&     �&     �&  *   �&  D   �&  )   /'  /   Y'  V   �'     �'     �'  	   �'  
   �'     (  !   (  S  -(     �)     �)     �)     �)  +   �)     �)     �)     �)  	   *     *     "*     +*     3*  $   B*  R   g*     �*     �*     �*     �*     �*     +     +     +     $+     ,+     9+     F+  "   f+     �+     �+     �+     �+     �+  %   �+  1   ,     :,     ?,     N,     T,  /   Z,     �,  '   �,     �,     �,  )   �,     -     -     -  
   "-     --  V   5-     �-     �-      �-     �-     �-  !   .  
   (.     3.     E.     U.     c.     h.     q.     �.     �.     �.     �.     �.     �.     /  
   6/     A/     V/     d/     k/  X   �/  X   �/     30     S0     Y0     `0     g0  
   �0     �0     �0     �0     �0     �0     �0     �0     �0     �0     �0     �0  	   �0     1     1  
   1     )1  *   ,1     W1     s1     �1     �1     �1     �1     �1     �1     �1  �   �1     �2  ,   �2  T   �2     %3  
   33  3  >3     r4     y4     �4     �4     �4  
   �4     �4     �4  
   �4     �4     �4     �4     �4     �4     5     5     %5  &   15     X5     ]5     d5     k5     |5     �5     �5     �5  l   �5     )6     16     56     C6     Q6     V6     \6     c6     i6     o6     �6     �6     �6     �6     �6     �6  8   �6  )    7  7   J7     �7     �7     �7  0   �7  "   �7  "   �7  >   8  B   L8  	   �8     �8  @   �8     �8  5   9  ;   G9  >   �9     �9     �9  &   �9  '    :  -   H:  K   v:     �:  	   �:     �:  <   �:  4   ;     F;     X;     d;      p;  &   �;     �;  w   �;  w   D<     �<     �<  p   �<  �  N=     �?     �?     @     #@     /@     3@  (   S@  )   |@  K   �@  &   �@  [   A  F   uA  0   �A  Y   �A  3   GB  Y   {B  0   �B  "   C     )C  #   AC     eC     {C     �C     �C     �C  f  �C     9E  	   OE  	   YE     cE     �E     �E     �E  	   �E     �E  	   �E     �E     �E  
   �E     �E     �E  
   �E     �E     �E     F     .F     7F  
   >F  
   IF  Z   TF     �F  !   �F  %   �F     G     G  M   #G     qG  	   �G     �G     �G     �G     �G     �G     �G     H     "H     5H     DH     SH  	   iH  	   sH     }H  "   �H  	   �H     �H     �H     �H     I  D  $I  v   iJ  �   �J  �   �K    tL     �M     �M     �M     �M     �M     �M     �M     �M     �M     �M     	N     N     %N     BN     _N     |N     �N     �N     �N  "   �N     �N     O  0   O     OO     TO     YO     aO     jO     oO  �   �O     zP     �P     �P     �P     �P     �P     �P  
   �P     �P     �P     �P     �P     �P     �P  1   Q     AQ     RQ  	   [Q  
   eQ     pQ     |Q     �Q     �Q     �Q     �Q     �Q     �Q     �Q     �Q     �Q     	R     R     R     (R      FR     gR     lR     yR     ~R      �R     �R  )   �R     �R     �R  )   �R     %S     *S     7S  
   >S     IS  /   QS     �S     �S     �S     �S     �S     �S     �S     T  
   T     T     'T     ,T  
   3T     >T     TT     jT     ~T     �T     �T     �T  
   �T     �T     �T     �T     �T  4   U  &   DU     kU     �U     �U     �U     �U  
   �U     �U     �U     �U  
   �U     �U     �U     �U     �U     �U     �U     �U     V     V     V     V     %V  "   (V     KV     cV     tV     yV     ~V     �V     �V  	   �V     �V  �   �V     DW     IW  2   ^W  
   �W     �W  �   �W     �X     �X  
   �X     �X     �X     �X     �X     �X     �X     	Y     Y     Y     Y     !Y     0Y     5Y     >Y     JY     cY     hY     qY     vY     �Y     �Y     �Y     �Y  <   �Y     �Y     �Y     �Y     Z     Z     Z     Z     Z     "Z     'Z     4Z     IZ     VZ     cZ     xZ     }Z  "   �Z  !   �Z  $   �Z     �Z     �Z     �Z  &   �Z     %[     6[  "   O[  $   r[     �[     �[  $   �[     �[      �[  '   \  -   6\     d\     y\     �\     �\     �\  /   �\     ]     ]     ]  )    ]      J]     k]     ~]     �]     �]     �]     �]  X   �]  X   &^     ^     �^  N   �^    �^     �`     �`     a     a     a     a     0a     Ka  ,   ja     �a  ;   �a  ,   �a      b  C   >b     �b  6   �b     �b     �b     c  "   c     <c     Lc     Yc     jc     }c                   �   �       Q   3   4   �   �         (           A   �   '      �   W       �   ,           .  6   �   �   �     0   �   �       �       �   �   *  �       �   �        �   h   �   �   �     �     �   �   �     ;   �   p   ]   d   �   �       �     �           )  �   <   O             �          "  ^   K   G       N   �   �   	                 �                           �   �   *   +            �   �   �   '       I   $             �   �   X       �   2   7  ~   1        �               �   +    �   i   �   
  	  /   v   3  c   �   �   m       a      >       o   8   �       R       �   9  �   L   �   �   �   �   J   r   �   0  �       �          �        �   F      �   �   �   #              u       �         �           �   P                   5     �       -           z   �         7   6  :   l   2      �   �   y                 %   8      (  �   M   �   �   /      �   ,  }   �               &  x       E   �      �      �       \      #  �   �   U   $       �   .   f           �           Y   )         �         B             5   �   �   �   1       �   �       �   �       b           �   _         �   g   "   =      D       �     �           %  �   T   H   [          �   
           k   9   �   `       Z   C   �   n   �           �   �                        t   �       �   �   �   �   �   �   �   {   !   e       w   �   �   -    s              j               &       |       �   �      q   �       @   �   4          �       S   �         �   �      �         �   �   :  !  ?   �       �   �   �       V                  Current printer:     About       EXIT      Choose which icons to hide or show:     View    Add   Available printers:   Browse...   Centered   Change...   Close   Delete   Desktop Color   Directory   Image   New Key   Reload Images   Select your desired keyboard repeat rate:   Select your desired mouse speed:   Select...   Set   Test Key   Tiled   is open source under the General Public License (GPL).
NO technical support will be provided for this application.
Enjoy!  on printer ' ' already exists and
triggers the program:  ' already exists.
Either edit or delete the existing ' ' into your PATH first. ' window settings. '.
If you are not happy with this mouse speed,
wait 7 seconds and the mouse speed will be
reset to a reasonable speed.
 '....Quitting. / _File / _File/_Configuration... / _File/_Quit / _File/_Refresh View / _File/sep4 /File/_Apply Changes Now... /File/_Exit /File/_Open... /File/_Save /File/sep1 /File/sep2 /Help/_About... /_File /_Help /_Help/_About /_Help/_About IceWM Control Panel... /_View /_View/_Icon View /_View/_List View A Gtk-based mouse configuration utility A check for the NEWEST version of this software will now be conducted. A simple Gtk-based control panel for IceWM - for people that prefer Gtk over the Qt-based 'IceCC'. It is modular, easy to use, with a Windows Control Panel like look. Although created for IceWM, it can be easily used as a control panel for any other window manager. It is highly configurable, and control panel applets can be created easily (simple text files).

This program is distributed under the GNU General Public License (open source). A simple Gtk-based key configuration utility
for IceWM (written in 100% Python).

This program is distributed free
of charge (open source) under the GNU
General Public License. A simple Gtk-based utility for setting the background color
and wallpaper settings in IceWM (written in 100% Python).
This program is distributed under the GNU General
Public License (open source).

WARNING: Some newer IceWM themes override the wallpaper and
background colors set here. If you see no changes, it may be that your
theme controls the background colors and images. A simple Gtk-based window options configuration utility
for IceWM (written in 100% Python).
Optimized for IceWM version 1.2.7.

This program is distributed free
of charge (open source) under the GNU
General Public License. A simple Windows-like or KDE-like clock management application
suitable for managing the clock in most window managers' system trays.
PhrozenClock was orginally created for use with the system tray clock in IceWM,
and was designed to be lightweight and less memory intensive than the KDE clock.

PhrozenClock GTK is open source under the General Public License (GPL). ADD About About  About IceWM CP - Key Editor About IceWM Control Panel AboveDock Add... Additional IceWM Shortcut Keys All rights reserved. Allow keyboard auto-repeat Allow keyboard beeps Allow keyboard clicks An error occurred while inserting this card. An error occurred while resetting this card. An error occurred while resuming this card. An error occurred while suspending this card. App is non-ICCCM compliant Apply Are you SURE you want to cancel print job  Are you sure you want to delete
the window options associated with:
 Are you sure you want to delete this key? Be sure that you are connected to the internet. Before reporting a bug, please make sure you have the NEWEST version of this software. Behavior Below Browse... Bug Report CANCEL CANCEL change and RESET the mouse CRITICAL ERROR

PhrozenClock was unable to determine the location
of your 'zoneinfo' time zone files, normally located in
 /usr/share/zoneinfo/.  You may have to edit the phrozenclock.py script and set
the ZONEINFO_DIR variable manually (sorry!).
Also, make sure that your /etc/localtime file is properly LINKED.

Critical Error: Quitting. Can be rolled up Cancel Cancel Print Job  Cancel Selected Print Job Check for newer versions of this program... Class Close Close and exit Closeable Column View Columns: Command Command to run Could not check for software update. Could not determine your timezone's 'nickname'.
You may have to set this yourself. Could not open Help file Credits Current Time Zone: Cursor Editor Date & Time Delay:  Delete Description: Desktop Do not cover Do not focus Do not focus when app is raised Do not show in Quick-switch window Do not show in Window List Do not show on taskbar Do you wish to continue? Dock Duration ERROR saving key configuration file:
 ERROR: Could not load available time zones from ' Edit Edit with Gimp Eject Empty Enter a list of icon paths, separated by colons Error Error backing up IceWM preferences to:
 Error loading card data Error saving. Error while saving IceWM preferences to:
 Files Force focusing From Geometry:  Grab... HINT: Use SINGLE clicks instead of double-clicks.
You will not see this message again. Has CLOSE button Has HIDE button (if applicable) Has LAYER button (if applicable) Has MAXIMIZE button Has MINIMIZE button Has ROLLUP button (if applicable) Has border Has resize border Has system menu Has title bar Hide Hideable Ice Control Panel IceWM CP - Cursor Editor IceWM CP - Key Editor IceWM CP - Keyboard Settings IceWM CP - Mouse Settings IceWM Control Panel IceWM Control Panel: Keyboard IceWM Control Panel: Mouse IceWM Keys IceWMCP Icon Browser Icon Paths... Icon:  Icons loaded.  Ready. If you are using an older version of this software, first upgrade to the NEWEST version. If you wish to help us with the translations, please visit the following site on the web Ignore pre-set window positions Info: Insert Job ID KEEP the new mouse speed Key Editor Key:   Keyboard Keyboard Beep Keyboard Click Keyboard Sounds Keys Layer:  Loading Maximizeable Menu Minimizeable Modified. Mouse Behavior Moveable NONE FOUND No No PCMCIA slots detected on this computer. No such file or directory:
 No such help topic Normal OK Ok OnTop Open Shortcut Key Configuration Owner/ID PC Card Manager Phrozen Clock will now be quickly RESTARTED.
This is absolutely necessary.

You may see a quick flicker on your display,
or may see the screen 'black' for a moment.
Do not be alarmed, as this is normal. Pitch Please describe the bug or problem in detail Please do not submit bug reports if you are using an older version of this software. Printer Queue Program:   PySpool is a simple Gtk-based spool monitor and manager written
in 100% Python. It is intended to monitor LPRng and LPR-based
printer spools. This is BETA code. PySpool has only been
tested with LPRng 3.8.12 on local printers.
PySpool requires the following executables on your PATH:
lpstat, cancel, checkpc Queue: Rate:  Ready. Refresh Queue Details Reload Repetition Reserve extra 'F[num]' keys Reset Resizeable Result Resume Run Run a program Run the selected command SELECT SEND NOW SMTP Server SORRY:
 Image could not
 be previewed. Save Saved. Select Select A Program Select Color Select Printer: Select a file... Send A Bug Report Send your bug report ONLY if you continue to have the same problem with the NEWEST version of this software. Server: Set Set Time Zone Shortcut Keys Size Slot  Slot   Sound Speed Start full-screen Start horizontally maximized Start maximized Start minimized Start vertically maximized State Status: Step 1: Open the program you wish to set properties for. Step 2: Click the 'Grab...' button below. Step 3: Click the crosshairs on the program you opened. Suspend Test Test:  The FROM e-mail address you provided is invalid. The executable file does not exist The executable is not on your PATH The following executable does not appear to be on your PATH:

 The following system information will be sent with your bug report The key ' The newest version available is The program this applet points to does not exist on your system. The wm_class/wm_name ' There is not enough information to send a bug report. There was an ERROR saving your window options to the file:
 This card has been ejected.
You may safely remove this device. This card has been inserted. This card has been reset. This card's activity has been resumed. This card's activity has been suspended This feature requires an internet connection. This key does not exist.
Please create it by clicking the 'New Key' button. Time Time Zone To To change the timezone,select your area from the list below: To test the repetition, hold a key for a few seconds Too large to tile Translators Tray Icon:  Unable to connect to SMTP server Unable to send your bug report message Unknown application Unknown error setting your timezone.
You may have to do this manually or edit the source for the phrozenclock.py script Unknown error writing your timezone.
You may have to do this manually or edit the source for the phrozenclock.py script Visible on all workspaces Volume WARNING:
This feature doesn't work perfectly on all systems.
BSD and some other systems may experience problems. WARNING: Changes in timezones do NOT
take affect in ALL applications immediately.

You may have to restart X or your window manager to
see the timezone change in clocks on your taskbar or system tray.

For IceWM users, you can just click 'Start' menu,
then select 'Logout',
then'Restart IceWM',
or re-apply your current IceWM theme.

For best results, you really should reboot your computer, or at
least re-start X. Some applications may appear to be 'stuck'
in the old timezone until the system is rebooted.
Running 'xrefresh' will NOT be enough. If you are running
mission critical or time-sensitive applications,
you should reboot immediately. You have been warned. Wallpaper Settings Window Options Window Options Editor WorkSpace:  Yes You are currently using version You can download the newest version from You can drag-and-drop icons where needed. You did not describe the bug or problem IN DETAIL in the 'bug report' area. You did not specify a valid SMTP host. You do not have the extra IceWMCPSystem program installed.  You must first download it from You may experience problems running PySpool properly.
Please install ' You must complete at least the 'wm_class' field. You must either delete the existing key,
or change the existing key's action using 'Set'. You must specify a program for this key combination Your SMTP server may require a password or may require you to check your POP3 mail first. Your bug report message was successfully sent to Your mouse speed has been set to ' description unavailable for IceWM (written in 100% Python). images...please wait. info unavailable queue status unavailable server status unavailable status unavailable Project-Id-Version: IceWMCP 2.4
POT-Creation-Date: Mon Apr 26 20:47:48 2003
PO-Revision-Date: 2003-06-19 17:09+0800
Last-Translator: Chao-Hsiung Liao <pesder.liao@msa.hinet.net>
Language-Team: Chinese (traditional) <zh-l10n@linux.org.tw>
MIME-Version: 1.0
Content-Type: text/plain; charset=Big5
Content-Transfer-Encoding: 8bit
Generated-By: pygettext.py 1.4
         �ثe���L����:    ����      ���}     ��ܭn���é���ܪ��ϥܡG     �˵�    �s�W   �i�Ϊ��L����:   �s��...   �m��   ����...   ����   �R��   �ୱ�C��   �ؿ�   �v��   �s�W����   ���s���J�v��   ��ܱz�Q�n����L���Ʋv�G  ��ܱz�Q�n���ƹ��t�סG ���...   �]�w   ���ի���   �ñ����   �O�̾ڳq�Τ������v(GPL)���}���l�X�n��C
�ڭ̤���o�����ε{�����ѧ޳N�䴩�C
�о����ɨ��I  ��L������ ���w�g�s�b�A�åB
�Ψ�Ĳ�o���{���G ���w�g�s�b�C
�z�i�H�s��ΧR���J�s���� �����[��z�����|�C �������]�w�C ���C
�p�G�z�����w�o�ӷƹ��t�סA
���� 7 �����A�ƹ��t�ױN�|
���]�����`���t�סC
 ��....���b���}�C /�ɮ�(_F) /�ɮ�(_F)/�պA(_C)... /�ɮ�(_F)/���}(_Q) /�ɮ�(_F)/��s�˵�(_R) /�ɮ�(_F)/seq4 /�ɮ�(_F)/�ߨ�M���ܧ�(_A)... /�ɮ�(_F)/���}(_E) /�ɮ�(_F)/�}��(_O)... /�ɮ�(_F)/�x�s(_S) /�ɮ�(_F)/sep1 /�ɮ�(_F)/sep2 /����(_H)/����(_A)... /�ɮ�(_F) /����(_H) /����(_H)/����(_A) /����(_H)/���� IceWM ����x(_A)... /�˵�(_V) /�˵�(_V)/�ϥ��˵�(_I) /�˵�(_V)/�M���˵�(_L) ��� Gtk ���ƹ��պA���ε{�� �{�b�|�ˬd�o�ӳn�骺�̷s���C ��� Gtk ��²�� IceWM ����x - �o�O�����w Gtk �ӹL Qt ������IceCC�����H�C���㦳�ҲդơB�e���ϥΩM�ݰ_�ӹ� Windows ����x���S�I�C���M�o�O�� IceWM �ҳ]�p���A���٬O�i�H�ܮe�����b�����L�����޲z�{����������x�ϥΡC���O���ץi�]�w���A�ӫإ߱���x�p�{���]�ܮe��(²�檺��r��)�C

�o�ӵ{���O�b GNU �q�Τ������v�U���G��(�}���l�X)�C ��� Gtk �� IceWM ²������պA
���ε{��(100% �H Python �g��)�C

�o�ӵ{���O�b GNU
�q�Τ������v�U
�K�O���G(�}���l�X)�C ��� Gtk ��²�� IceWM �I���C��P���ȳ]�w
���ε{��(100% �H Python �g��)�C
�o�ӵ{���O�b GNU �q�Τ������v
�U���G��(�}�񷽩l�X)�C

ĵ�i�G�@�Ǹ��s�� IceWM �D�D�|�\�L�o�z��
���ȻP�I���C��]�w�C�p�G�z�ݤ�������ܤơA
�i��O�z���D�D����F�I���C��P�v���C ��� Gtk ��²�� IceWM �����ﶵ�պA
���ε{��(100% �H Python �g��)�C
�w�� IceWM version 1.2.7 �̨ΤơC

�o�ӵ{���O�b GNU
�q�Τ������v�U
�K�O���G(�}�񷽩l�X)�C �@��²�檺�� Windows ���� KDE �������޲z���ε{���A
�A�Ω�޲z�j���������޲z�{�����t�ΧX�������C
PhrozenClock �쥻�O���F�Φb IceWM ���t�ΧX�����ӫإߪ��A
�ҥH�����]�p�O���q�Ū��A�åB�ϥΪ��O����]�� KDE �����֡C

PhrozenClock GTK �O�̾ڳq�Τ������v(GPL)���}���l�X�n��C �s�W ���� ����  ���� IceWM CP - ����s�边 ���� IceWM ����x �b���a�Ϥ��W �s�W... �B�~�� IceWM �ֱ��� �O�d�Ҧ��v�Q�C ���\��L�۰ʭ��� ���\��L���n ���\��L�V���n �b���J�o���X�R�d�ɵo�Ϳ��~�C �b���]�o���X�R�d�ɵo�Ϳ��~�C �b��_�o���X�R�d�ɵo�Ͱ��D�C �b�Ȱ��o���X�R�d�ɵo�Ϳ��~ ���ε{�����ۮe ICCCM �M�� �z�T�w�n�����C�L�u�@  �z�T�w�n�R��
�P�����p�������ﶵ�G
 �z�T�w�n�R���o�ӫ���ܡH �нT�w�z�w�s�u����ں����C �b���i���~���e�A�нT�w�z���n��w�g�O�̷s�������C �欰 �U�� �s��... ���~�^�� ���� �����ܧ�í��]�ƹ� �Y�������~

PhrozenClock �L�k�T�w�z��
��zoneinfo���ɰ��ɮת���m
(�q�`��� /usr/share/zoneinfo/)�C�z�i��ݭn�s�� phrozenclock.py �R�O�Z��
��ʳ]�w ZONEINFO_DIR �ܼ�(��p�I)�C
�P�ɡA�нT�w�z�� /etc/localtime �ɮפw���T�s���C

�Y�������~�G���b���}�C �i�H���_ ���� �����C�L�u�@  ������ܪ��C�L�u�@ �ˬd���{�����s����... ���O ���� ���������} �i���� ���˵� ��: ���O �n���檺���O �L�k�ˬd�n���s���ΡC �L�k�T�w�z�ɰϪ����ʺ١��C
�z�i��ݭn�ۦ�]�w���C �L�k�}�һ����ɮ� �u�@�H�� �ثe�ɰ�: ��нs�边 ��� & �ɶ� ����:  �R�� �y��: �ୱ �����л\ ���n�J�I �����ε{�����ɮɤ��������o�J�I ���n�b���ֳt��������������� ���n�b�����M�椤��� ���n�b�u�@�C�W��� �z�Ʊ��~��ܡH ���a�� ����ɶ� �x�s����պA�ɮ׮ɵo�Ϳ��~�G
 ���~�G�L�k�󦹸��J�i�Ϊ��ɰϡG�� �s�� �H Gimp �s�� �h�X �Ū� ��J�ϥܸ��|���M��A�i�Ϋ_�����j ���~ �ƥ� IceWM preferences �즹�B�ɵo�Ϳ��~:
 ���J�X�R�d��Ʈɵo�Ϳ��~ �x�s�o�Ϳ��~�C �x�s IceWM preferences �즹�B�ɵo�Ϳ��~:
 �ɮ� �j����o�J�I �H��H �j�p��m:  ���... ���ܡG�ϥγ����N�������C
�z�N���|�A�ݨ즹�T���C �������������s �������á����s(�p�G�A�X) �����h�������s(�p�G�A�X) �����̤j�ơ����s �����̤p�ơ����s �������_�����s(�p�G�A�X) ����� �����ܤj�p��� ���t�ο�� �����D�C ���� �i���� Ice ����x IceWM CP - ��нs�边 IceWM CP - ����s�边 IceWM CP - ��L�]�w IceWM CP - �ƹ��]�w IceWM ����x IceWM ����x�G��L IceWM ����x�G�ƹ� IceWM ���� IceWMCP �ϥ��s���� �ϥܸ��|... �ϥ�:  �w���J�ϥܡC  �ǳƧ����C �p�G�z�b�ϥΦ��n�骺���ª����A�����ФɯŦ̷ܳs�����C �p�G�z�Ʊ����U�ڭ�½Ķ�A�аѳX�U�C���} �����w���]�w��������m ��T: ���J �u�@ ID �O���s���ƹ��t�� ����s�边 ����:   ��L ��L���n ��L�V���n ��L�n�� ���� �h��:  ���J�� �i�̤j�� ��� �i�̤p�� �w�ק�C �ƹ��欰 �i���� �䤣�� �_ �b�o�O�q���W�������� PCMCIA ���ѡC �S���o�˪��ɮשΥؿ��G
 �S���o�ӻ����D�D �@�� �T�w �T�w �m�󳻺� �}�ҧֱ���պA �Ҧ��H/ID PC Card �޲z�{�� Phrozen Clock �Y�N���s�ҰʡC
�o�O���沈�n���C

�z�i�|�ݨ�z����ܾ��ֳt�{�ʡA
�Ϊ̥i��ݨ�ù����ťա��@�|��C
�Ф��ε۫�A�]���o�O���`�{�H�C ���� �иԲӴy�z���~�ΰ��D �p�G�z�b�ϥΦ��n�骺���ª����A�Ф��n�ǰe���~���i�C �L������C �{��:   PySpool �O��� Gtk ��²�� spool �ʵ����P�޲z�{���A
�åB�O 100% �H Python �g���C���ΨӺʱ� LPRng �M��� LPR ��
�L���� spool�C�o�O���դ����{���CPySpool �u��
�����L������ LPRng 3.8.12 ���L���աC
PySpool �ݭn�z�����|�]�t�U�C�i�����ɡG
lpstat, cancel, checkpc ��C: �W�v:  �ǳƴN���C ���s��z��C�ԲӸ�T ���s���J ���� �O�d�B�~����F[num]������ ���] �i�H���ܤj�p ���G �~�� ���� ����{�� �����ܪ����O ��� �ߧY�e�X SMTP ���A�� ��p�G
 �L�k�w��
 �v���C �x�s �w�x�s�C ��� ��ܤ@�ӵ{�� ����C�� ��ܦL����: ����ɮ�... �e�X���~�^�� �u�����z�b�̷s���n�餤����o�ͬۼ˰��D�ɤ~�ݭn�e�X���~���i�C ���A��: �]�w �]�w�ɰ� �ֱ��� �j�p ����  ����   �n�� �t�� �H���ù��Ұ� �H������V�̤j�ƱҰ� �H�̤j�ƱҰ� �H�̤p�ƱҰ� �H������V�̤j�ƱҰ� ���A ���A: �B�J 1�G�}�ұz�Q�n�]�w���e���{���C �B�J 2�G���U�誺�����...�����s�C �B�J 3�G���U�z�Ҷ}�ҵ{�����Q�r�ǽu�C �Ȱ� ���� ����:  �z�Ҵ��Ѫ��H��H�q�l�l���}�O�L�Ī��C �ӥi�����ɤ��s�b �ӥi�����ɤ��b�z�����|�� �U�C�i�����ɦ��G���b�z�����|���G

 �U�C���t�θ�T�|�H�۱z�����~���i�e�X ���䡧 �{���̷s�������O �o�Ӥp�{�����V���{�����b�z���t�Τ��C �o�� wm_class/wm_name �� �S����������T�i�H�e�X���~���i�C �N�z�������ﶵ�x�s�즹�ɮ׮ɵo�Ϳ��~�G
 �o���X�R�d�w�h�X�C
�z�i�H�w���������o���˸m�C �o���X�R�d�w�g���J�C �o���X�R�d�w���]�C �o���X�R�d�����ʤw��_�C �o���X�R�d�����ʤw�g�Q�Ȱ��F �o�ӥ\��ݭn�s�u����ں����C �o�ӫ��䤣�s�b�C
�Ы����s�W���䡨���s�ӫإߥ��C �ɶ� �ɰ� ����H �n���ܮɰϡA�бq�U�����M�椤��ܱz���ϰ�: �n���խ��Ƴt�סA�Ы�������X���� �Ӥj�ӵL�k�ñ���� ½Ķ�� �X�ϥ�:  �L�k�s�u�� SMTP ���A�� �L�k�e�X�z�����~���i�T�� ���������ε{�� �]�w�z���ɰϮɵo�ͥ������~�C
�z�i��ݭn��ʧ������άO�s�� phrozenclock.py �R�O�Z����l�X �g�J�z���ɰϮɵo�ͥ������~�C
�z�i��ݭn��ʧ������άO�s�� phrozenclock.py �R�O�Z����l�X �b�Ҧ��u�@�ϳ��O�i���� ���q ĵ�i�G
�o�ӥ\��ëD�b�Ҧ��t�γ��ॿ�`�B�@�C
BSD �M��L�����t�Υi��|�J����D�C ĵ�i�G�b�ɰϤ����ܧ󤣷|
�ߨ�b�Ҧ����ε{�����ͮġC

�z�i��ݭn���s�Ұ� X �αz�������޲z�{��
�~��ݨ�ɰϹ�z�u�@�C�Ψt�ΧX�W���������v�T�C

��� IceWM �ϥΪ̡A�z�i�H���U���}�l�����A
���ۿ�ܡ��n�X���A
�M�����s�Ұ� IceWM���A
�Ϊ̭��s�M�αz�ثe�� IceWM �D�D�C

���D�̦n���ĪG�A�z���ӽT�꭫�s�Ұʹq���A�Ϊ�
�̤֤]���s�Ұ� X�C�������ε{���i��|���O�Q���v��
�b�ª��ɰϡA�@����t�έ��s�Ұʬ���C
���桧xrefresh���i���٤����C�p�G�z���b����
���n���@�~�άO��ɶ��ܱӷP�����ε{���A
�z���ӥߧY���s�}���C�z�w�g�Qĵ�i�L�F�C ���ȳ]�w �����ﶵ �����ﶵ�s�边 �u�@��:  �O �z�ثe�ϥΪ������O �z�i�H�q�o�z�U���̷s�����G �z�i�H�N�ϥܩ���ݭn���a��C �z�b�����~���i���ϨèS���ԲӴy�z���~�ΰ��D�C �z�S�����w���Ī� SMTP �D���C �z�S���w���B�~�� IceWMCPSystem �{���C  �z�������b�o�̤U���G �z�b���� PySpool �ɥi��|�J����D�C
�Цw�ˡ� �z�ܤ֥���������wm_class�����C �z�����R���o�ӬJ�s������A
�άO�ϥΡ��]�w���ӧ��ܦ��J�s���䪺�ʧ@�C �z�������o�ӲզX����w�@�ӵ{�� �z�� SMTP ���A���i��ݭn�K�X�λݭn�z���ˬd POP3 �l��C �z�����~���i�T���w���\�ǰe�� �z���ƹ��t�פw�g�]���� �L�k���o�y�z �Ω� IceWM (100% �H Python �g��)�C �v��...�еy�ԡC �L�k���o��T �L�k���o��C���A �L�k���o���A�����A �L�k���o���A 